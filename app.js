let footer = new Date().getFullYear().toString()+'<sup>&copy;</sup>';

new Vue({
    data: {
        app_name: 'Company Name',
        header: 'COMING SOON',
        message: '',
        date: 'Feb 30, 2019 15:37:25',
        footer_text: footer
    },

    mounted() {
        let data = new Date(this.date).getTime();
        let self = this;

        let interval = setInterval(function () {
            let now = new Date().getTime();
            let distance = data - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            self.message = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            if (distance < 0) {
                clearInterval(interval);
                self.message = "EXPIRED";
            }
        }, 1000);
    }
}).$mount('#appEngine');